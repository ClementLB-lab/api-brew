import * as S from "sequelize-typescript"

/**
 * A registered Brew user
 */
@S.Table
export default class DBUser extends S.Model<DBUser> {

    @S.PrimaryKey
    @S.AutoIncrement
    @S.Column(S.DataType.INTEGER)
    id: number

    @S.Column(S.DataType.TEXT)
    firstname: string

    @S.Column(S.DataType.TEXT)
    name: string

    @S.Column(S.DataType.TEXT)
    email: string

    @S.Comment("hashed and salted")
    @S.Column(S.DataType.TEXT)
    passwordHash: string

    @S.Default(false)
    @S.Column(S.DataType.BOOLEAN)
    confirmed: boolean
}
