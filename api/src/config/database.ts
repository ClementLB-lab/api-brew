import { Sequelize } from 'sequelize-typescript'
import util from 'util'

require('dotenv').config()

let PORT: number = process.env.APP_TARGET == "test"
                    ? +process.env.TEST_DB_PORT
                    : +process.env.DEV_DB_PORT

let HOST: string = process.env.DB_HOST || undefined


export let db = new Sequelize({
    host: HOST,
    username: "user",
    password: "password",
    dialect: "postgres",
    database: "user",
    port: PORT || 5432,

    storage: ":memory:",
    models: [__dirname + '/../dbmodels', __dirname + '/../dbmodels/through_table'],
    logging: false
});

export async function onStartup() {
    console.log("Connecting to '" + HOST + "' on port " + PORT + " || 5432")
    console.log("Waiting for db connection")

    await db.authenticate()
        .then(() => {
            console.log('Connection to database has been established successfully.');
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
        });

    // await db.sync({force: true, }) // - If you need to apply non-retrocompatible changes (will clear the db)
    await db.sync({alter: true})
}
