import DBUser from "../dbmodels/User";
import util from "util";

export default class UserManager {

    public async getByEmail(email: string): Promise<DBUser> {
        return DBUser.findOne({ where: { email: email } })
    }

    public async getById(userId: number): Promise<DBUser> {
        return DBUser.findOne({ where: { id: userId } })
    }

    public async create(firstname: string, name: string, email: string, passwordHash: string): Promise<DBUser> {
        return DBUser.create({ firstname, name, email, passwordHash })
    }

    public async activate(user: any): Promise<void> {
        user.confirmed = true

        await user.save()
    }

    public async updatePassword(email: string, passwordHash: string): Promise<void> {
        let user = await this.getByEmail(email)

        user.passwordHash = passwordHash

        await user.save()
    }
    
    public async update(user: any, firstname: string, name: string, email: string): Promise<void> {
        user.firstname = firstname
        user.name = name
        user.email = email

        await user.save()
    }

    public async delete(user: any): Promise<void> {
        await DBUser.destroy({ where: { id: user.id } })
    }
}