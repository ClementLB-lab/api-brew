/*
**  NotificationService
**
**  This service is used to send some mails
*/

let nodemailer = require('nodemailer');

let accountId = 'brew4.help@gmail.com'
let accountPwd = 'ywV73Rz5N'

let transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: accountId,
		pass: accountPwd
	}
});

export const sendConfirmationEmail = (user, token) => {
	if (!user || !token)
		return;
	
	let link = `http://localhost/user/confirmation/${token}`
	let mailOptions = {
		from: accountId,
		to: user.email,
		subject: '[Brew] Please confirm your email address',
		html: `
		<p>Hey ${user.firstname}!</p>

		<p>Thanks for joining Brew. To finish registration, please click the button below to verify your account.</p>

		<a href=${link}>${link}</a>

		<p>Once verified, you can take full advantage of our services. If you have any problems, please contact us: brew4.help@gmail.com</p>

		<p></p><p>- Brew Team</p>`
	};

    transporter.sendMail(mailOptions, function(error, info) {
		if (error) {
			console.log(error);
		} else {
			console.log('Email sent: ' + info.response);
		}
	});
}

export const sendResetPasswordEmail = (user, url) => {
	if (!user || !url)
		return;

	let mailOptions = {
		from: accountId,
		to: user.email,
		subject: 'Reset password instructions',
		html: `
		<p>Hello ${user.name || user.email},</p>

		<p>Someone, hopefully you, has requested to reset the password for your Brew account.</p>

		<p>If you did not perform this request, you can safely ignore this email.</p>
		
		<p>Otherwise, click the link below to complete the process.</p>
		
		<a href=${url}>${url}</a>
		<p></p><p>- Brew Team</p>`
	};

	transporter.sendMail(mailOptions, function(error, info) {
		if (error) {
			console.log(error);
		} else {
			console.log('Email sent: ' + info.response);
		}
	});
}

export const getPasswordResetURL = (user, token) =>
  `http://localhost/resetpwd/${user.id}/${token}`