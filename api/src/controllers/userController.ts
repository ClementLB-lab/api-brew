import { Request, Response } from 'express'
import UserService from '../services/userService';
import * as jwt from "../utils/jwt"

import util from 'util'

export default class UserController {

    private userService = new UserService()

    /**
     * GET
     *
     * query {
     *      token: authentification JWT
     * }
     *
     * return:
     * 200 - data : User
     */
    public async getByJWT(req: Request, res: Response) {
        const jwt = req.query.token as string

        const user = await this.userService.getByJWT(jwt)

        return res.status(200).json(user)
    }


    /**
     * GET
     *
     * query {
     *      id: User id
     *      token: authentification JWT
     * }
     *
     * return:
     * 200 -
     *  data :
     *      If retrieving information from another user, get public field:
     *          name, rating, ratingCounts, id
     *      otherwise get all fields
     */
    public async getById(req: Request, res: Response) {
        const id = parseInt(req.query.id as string);
        const token = req.query.token as string;

        const currentUser = await this.userService.getByJWT(token)
        const user = await this.userService.getById(id)

        let result: any; // TODO: type User
        if (user && (!currentUser || user.id != currentUser.id)) { // Only get public fields from other profiles
            result = {
                name: user.name,
                id: user.id,
                firstname: user.firstname,
                email: user.email,
            }
        } else
            result = user

        return res.status(200).json(result)
    }

    /**
     * GET
     *
     * query {
     *      token: JWT
     * }
     *
     * return:
     * 200 - data : User
     */
    public async confirmation(req: Request, res: Response) {
        const jwt = req.query.token as string

        const result = await this.userService.confirmed(jwt)
        
        if (result.isSuccessful())
            return res.status(200).json({ success: true, token: result.getData() })
        else
            return res.status(200).json({ success: false, errors: result.getError() })
    }

    /**
     * POST
     *
     * Registers the new user
     *
     * body {
     *  firstname: The firstname
     *  name : The name
     *  email : The email
     *  password : The password
     * }
     *
     * 200 -
     *  data : {
     *      success: whether the user has been registered
     *      errors {
     *          For each field containing error :
     *          [field name] : [error message (FR)]
     *      }
     * }
     */
    public async register(req: Request, res: Response) {
       const { firstname, name, email, password } = req.body;

        const result = await this.userService.register(firstname, name, email, password)

        if (result.isSuccessful())
            return res.status(200).json({ success: true, token: result.getData() })
        else
            return res.status(200).json({ success: false, errors: result.getError() })
    }

    /**
     * POST
     *
     * Login as the given user
     *
     * body {
     *  email : The email
     *  password : The password
     * }
     *
     * 200 -
     *  data : {
     *      success: whether the user has been registered
     *      token: The generated JWT (in case of success)
     *      errors {
     *          For each field containing error :
     *          [field name] : [error message (FR)]
     *      }
     * }
     */
    public async login(req: Request, res: Response) {
       const { email, password } = req.body;

        const result = await this.userService.login(email, password)

        if (result.isSuccessful())
            return res.status(200).json({ success: true, token: result.getData() })
        else
            return res.status(200).json({ success: false, errors: result.getError() })
    }

    /**
     * POST
     *
     * Send an email if user's email match
     *
     * body {
     *  email : The email
     * }
     *
     * 200 -
     *  data : {
     *      success: whether the user has been registered
     *      fieldsErrors {
     *          For each field containing error :
     *          [field name] : [error message (FR)]
     *      }
     * }
     */
    public async forgotpwd(req: Request, res: Response) {
        const { email } = req.body;

        const result = await this.userService.forgotPwd(email)

        if (result.isSuccessful())
            return res.status(200).json({ success: true })
        else
            return res.status(200).json({ success: false, fieldsErrors: result.getError() })
    }

    /**
     * POST
     *
     * Reset the password's user
     *
     * body {
     *  password : The password
     *  password2 : The password2
     * }
     *
     * 200 -
     *  data : {
     *      success: whether the user has been registered
     *      fieldsErrors {
     *          For each field containing error :
     *          [field name] : [error message (FR)]
     *      }
     * }
     */
    public async resetpwd(req: Request, res: Response) {
        const { password, password2, id, token } = req.body;

        const result = await this.userService.resetPwd(password, password2, id, token)

        if (result.isSuccessful())
            return res.status(200).json({ success: true })
        else
            return res.status(200).json({ success: false, fieldsErrors: result.getError() })
    }

    /**
     * POST
     *
     * Reset the current password's user
     *
     * body {
     *  token: The JWT auth token
     *  currentpassword : The current password
     *  password : The password
     *  password2 : The password2
     * }
     *
     * 200 -
     *  data : {
     *      success: whether the user has been registered
     *      fieldsErrors {
     *          For each field containing error :
     *          [field name] : [error message (FR)]
     *      }
     * }
     */
    public async changePwd(req: Request, res: Response) {
        const { token, currentpassword, password, password2 } = req.body;

        const result = await this.userService.changePwd(token, currentpassword, password, password2)

        if (result.isSuccessful())
            return res.status(200).json({ success: true })
        else
            return res.status(200).json({ success: false, fieldsErrors: result.getError() })
    }

    /**
     * POST
     *
     * Update user information.
     *
     * body {
     *  firstname: string
     *  name : string
     *  email : string
     *  token: The JWT auth token
     * }
     *
     * Return :
     * 200 - data {
     *      success: whether the account has successfully be upgraded
     *      err: Potential error message (FR)
     * }
     *
     */
    public async update(req: Request, res: Response) {
        const { firstname, name, email, token } = req.body;

        const result = await this.userService.update(firstname, name, email, token)

        if (result.isSuccessful())
            return res.status(200).json({ success: true });
        else
            return res.status(200).json({ success: false, err: result.getError() })
    }

    /**
     * POST
     *
     * Deletes account user
     *
     * body {
     *  reason: string
     *  suggestion: string
     *  token: The JWT auth token
     * }
     *
     * Return :
     * 200 - data {
     *      success: whether the account has successfully be deleted
     *      err: Potential error message (FR)
     * }
     *
     */
    public async deleteAccount(req: Request, res: Response) {
        const { reason, suggestion, token } = req.body;

        const result = await this.userService.deleteAccount(reason, suggestion, token)

        if (result.isSuccessful())
            return res.status(200).json({ success: true });
        else
            return res.status(200).json({ success: false, err: result.getError() })
    }
}
