import * as jwt from '../utils/jwt';
import UserManager from '../manager/userManager';
import DBUser from '../dbmodels/User';
import validator from 'validator';
import Result from '../utils/result';
import * as pwdUtil from '../utils/password';
import * as emailUtil from '../utils/email';

export default class UserService {

    private userManager = new UserManager()

    /**
     * Gets a user from a JWT
     * @return the user or null if jwt is invalid
     */
    public async getByJWT(token: jwt.Token): Promise<DBUser> {
        if (!token)
            return null
        return this.getById(jwt.getUserIdFromToken(token));
    }

    /**
     * Gets a user from an ID
     * @return the user or null if no such user exists
     */
    public async getById(id: number, fields?: string): Promise<DBUser> {
        if (!id)
            return null

        const result = await this.userManager.getById(id);

        return result
    }

    /**
     * Checks if an account has been already confirmed
     * @return the user or null if jwt is invalid
     */
    public async confirmed(token: jwt.Token): Promise<Result> {
        if (!token)
            return null
        
        const user = await this.getById(jwt.getUserIdFromToken(token))

        if (!user)
            return Result.error("Your link is not or no longer valid")
        if (user.confirmed)
            return Result.error("This account has been already activated")

        this.userManager.activate(user)
        return Result.success()
    }

    /**
     * Creates a new user with these field.
     * @param password The password in clear, it will hashed and salted
     */
    public async register(firstname: string, name: string, email: string, password: string): Promise<Result<jwt.Token, Object>> {
        let fieldsErrors: any = {};

        if (!firstname || !validator.isLength(firstname, { min: 2 }))
            fieldsErrors.firstname = "The first and last name need to have at least two letters each"
        if (!name || !validator.isLength(name, { min: 2 }))
            fieldsErrors.name = "The first and last name need to have at least two letters each"
        if (!email || !validator.isEmail(email))
            fieldsErrors.email = "Unknown Email"
        if (!password || !validator.isLength(password, { min: 8 }))
            fieldsErrors.password = "The new password must contain at least 8 characters"

        if (Object.keys(fieldsErrors).length)
            return Result.error(fieldsErrors)

        if (await this.userManager.getByEmail(email))
            return Result.error("Email already used")

        const createdUser = await this.userManager.create(firstname, name, email, pwdUtil.hash(password))

        if (!createdUser)
            return Result.error("Your account registration failed")

        const token = jwt.getTokenForUser(createdUser)
        emailUtil.sendConfirmationEmail(createdUser, token)

        return Result.success(token)
    }

    /**
     * Verify that the email and password match, if so generates a JWT
     *
     * @param password The password in clear
     */
    public async login(email: string, password: string): Promise<Result<jwt.Token, Object>> {

        const user = await this.userManager.getByEmail(email)

        if (!user)
            return Result.error({ email: "Unknown Email" })

        if (!user.confirmed)
            return Result.error({ confirmed: "Please confirm your email address using the link sent to you at the following address : " + email })
    
        const match = await pwdUtil.checkMatch(password, user.passwordHash)
        if (!match)
            return Result.error({ password: "Incorrect Password" })

        return Result.success(jwt.getTokenForUser(user))
    }

    /**
     * Verify that the email match
     *
     * @param email The email to find
     */
    public async forgotPwd(email: string): Promise<Result<jwt.Token, Object>> {

        if (!email)
            return Result.error("Invalid Address")

        const user = await this.userManager.getByEmail(email)
        if (!user)
            return Result.error({ email: "Unknown Email" })
        const token = jwt.usePasswordHashToMakeToken(user)
        const url = emailUtil.getPasswordResetURL(user, token)
        const emailTemplate = emailUtil.sendResetPasswordEmail(user, url)

        return Result.success()
    }

    /**
     * Allows you to change your password EVEN if you NO LONGER know your current password.
     *
     * @param newPassword The new password 
     * @param confirmPassword Just a confirmation of the new password 
     * @param id The user ID
     * @param token The JWT of the user
     * 
     * @return the error message or success
     */
    public async resetPwd(newPassword: string, confirmPassword: string, id: number, token: string): Promise<Result> {

        if (!id || !token)
            return Result.error("An error occurred when sending some data")

        let fieldsErrors: any = {};

        if (!newPassword || !validator.isLength(newPassword, { min: 8 }))
            fieldsErrors.newPassword = "The new password must contain at least 8 characters"

        if (newPassword != confirmPassword)
            fieldsErrors.confirmPassword = "The new password must be correctly confirmed"

        if (Object.keys(fieldsErrors).length)
            return Result.error(fieldsErrors)

        const user = await this.userManager.getById(id);

        if (!user)
            return Result.error("Sorry. We didn't find your account")

        const ret = jwt.decodeToken(user, token)

        if (ret == null)
            return Result.error("The token is either expired or invalid")

        if (ret === user.id) {
            await this.userManager.updatePassword(user.email, pwdUtil.hash(newPassword))
            return Result.success()
        }
        return Result.error("There seems to be a problem with your ID")
    }

    /**
     * Allows you to change your password if you know your current password.
     *
     * @param currentPassword The current password 
     * @param newPassword The new password 
     * @param confirmPassword Just a confirmation of the new password 
     * @param token The JWT of the user
     * 
     * @return the error message or success
     */
    public async changePwd(token: jwt.Token, currentPassword: string, newPassword: string, confirmPassword: string): Promise<Result<jwt.Token, Object>> {

        if (!currentPassword || !newPassword || !confirmPassword)
            return Result.error("You have not completed all the mandatory fields")
        
        let fieldsErrors: any = {};

        if (currentPassword === newPassword)
            fieldsErrors.newPassword = "You must choose a different password from the last one"
        else if (!validator.isLength(newPassword, { min: 8 }))
            fieldsErrors.newPassword = "The new password must contain at least 8 characters"
        else if (newPassword != confirmPassword)
            fieldsErrors.confirmPassword = "The new password must be correctly confirmed"

        if (Object.keys(fieldsErrors).length)
            return Result.error(fieldsErrors)

        const user = await this.getByJWT(token)
        console.log(user.id + " and " + user.firstname + " " + user.name)

        if (!user)
            return Result.error("Sorry. We didn't find your account")

//        const match = await pwdUtil.checkMatch(currentPassword, user.passwordHash)
        if (!await pwdUtil.checkMatch(currentPassword, user.passwordHash))
            return Result.error({ currentPassword: "Incorrect Password" })

        await this.userManager.updatePassword(user.email, pwdUtil.hash(newPassword))
        return Result.success()
    }

    /**
     * Update User information
     *
     * @param firstname The user's first name 
     * @param name The user's name 
     * @param email The user's email 
     * @param token The token of the user
     * 
     * @return the error message or success
     */
    public async update(firstname: string, name: string, email: string, token: jwt.Token): Promise<Result> {
        const user = await this.getByJWT(token)

        if (!user)
            return Result.error("Unable to retrieve your profile. Please try again later")
        if (firstname == '' || !email || !validator.isEmail(email))
            return Result.error("You must at least keep a name and a valid email address")
        if (!name || !validator.isLength(name, { min: 3 }))
            return Result.error("The name must contain at least 3 characters")

        await this.userManager.update(user, firstname, name, email)
        return Result.success()
    }
    
    /**
     * Deletes Account User
     *
     * @param reason The user's first name 
     * @param suggestion The user's name 
     * @param token The token of the user
     * 
     * @return the error message or success
     */
    public async deleteAccount(reason: string, suggestion: string, token: jwt.Token): Promise<Result> {
        const user = await this.getByJWT(token)

        /* 
         * The 'reasons' and 'suggestions' fields are not used at the moment.
         * However, we will soon be offering the user to provide us with some
         * suggestions, as well as the reason for leaving.
        */

        if (!user)
            return Result.error("Unable to retrieve your profile from your login token.")
        await this.userManager.delete(user)
        return Result.success()
    }
}
