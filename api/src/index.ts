import App from "./app"
import * as db from "./config/database"


async function start() {
/*
    //    await db.sync({force: true}) // - If you need to apply non-retrocompatible changes (will clear the db)
    await db.sync({alter: true})
*/

    await db.onStartup()

    let servApp = new App()

    servApp.app.listen(3001, function () {
        console.log("API started, listening on port 3001...")
    });
}

start();
