import express from 'express';
import * as routerUtil from './utils/routerUtil'

import UserController from './controllers/userController'


export default class Route {
    private userController = new UserController()

    public Route(app: express.Application) {

        //USER
        app.route("/user/getByJWT").get(async (req, res, next) => await this.userController.getByJWT(req, res).catch(next))
        app.route("/user/getById").get(async (req, res, next) => await this.userController.getById(req, res).catch(next))
        app.route("/user/confirmation").get(async (req, res, next) => await this.userController.confirmation(req, res).catch(next))
        app.route("/user/register").post(async (req, res, next) => await this.userController.register(req, res).catch(next))
        app.route("/user/login").post(async (req, res, next) => await this.userController.login(req, res).catch(next))
        app.route("/user/forgotPwd").post(async (req, res, next) => await this.userController.forgotpwd(req, res).catch(next))
        app.route("/user/resetPwd").post(async (req, res, next) => await this.userController.resetpwd(req, res).catch(next))
        app.route("/user/changePwd").post(async (req, res, next) => await this.userController.changePwd(req, res).catch(next))
        app.route("/user/update").post(async (req, res, next) => await this.userController.update(req, res).catch(next))
        app.route("/user/deleteAccount").post(async (req, res, next) => await this.userController.deleteAccount(req, res).catch(next))
    }

    // Not working : context loss

    // private wrapper(func: () => (req: express.Request, res: express.Response, next?: express.NextFunction) => Promise<any>) {
    //     return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    //         return await func()(req, res, next).catch(next)
    //     };
    // }
}
