import App from "../src/app"
import * as db from "../src/config/database"


////////////////////////////////////////////////////////////////////////////////
//
// TESTS SETUP
//
////////////////////////////////////////////////////////////////////////////////

global.console.log = jest.fn() // Ignore console.log in tests

let brewApp: App = null


beforeAll(async () => {
    console.log("calling db.onStartup")

    brewApp = new App
    console.log("BrewApi created")

    try {
        await db.onStartup()

    } catch (error) {
        console.error(error)
    }

    console.log("beforeAll setup complete")
})

afterEach(async () => {
    await db.db.sync({ force: true, })
})


////////////////////////////////////////////////////////////////////////////////

import request from 'supertest'
import UserManager from "../src/manager/userManager"
import util from "util"
import * as jwtUtil from "../src/utils/jwt"
import DBUser from "../src/dbmodels/User"
import { createDecipher } from "crypto"

////////////////////////////////////////////////////////////////////////////////
//
// UTILS
//
////////////////////////////////////////////////////////////////////////////////

