npm install

echo "Starting tests"

APP_TARGET=test jest --coverage --detectOpenHandles

if [ $? -eq 0 ]
then
    echo "Tests successfully passed"
    exit 0
else
    echo "Error : One or some tests not passed"
    exit 1
fi
